const Discord = (window as any).Discord;

export const updateCurrentClient = (newState: Discord.Client | null) => {
    return {
        type: "UPDATE_CURRENT_CLIENT",
        payload: newState,
    };
};

export default updateCurrentClient;
