const Discord = (window as any).Discord;

export const updateCurrentRecipient = (newState: Discord.User | null) => {
    return {
        type: "UPDATE_CURRENT_RECIPIENT",
        payload: newState,
    };
};

export default updateCurrentRecipient;
