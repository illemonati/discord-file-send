export const updateIsHost = (newState: boolean) => {
    return {
        type: "UPDATE_IS_HOST",
        payload: newState,
    };
};

export default updateIsHost;
