import {
    Box,
    Container,
    createMuiTheme,
    CssBaseline,
    ThemeProvider,
} from "@material-ui/core";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import AppBarComponent from "./components/AppBarComponent/AppBarComponent";
import RouterComponent from "./Router";

const theme = createMuiTheme({
    palette: {
        type: "dark",
        primary: {
            main: "#7289DA",
        },
        background: {
            default: "#202225",
            paper: "#2f3136",
        },
        action: {
            selected: "#393c42",
            hover: "rgba(79,84,92,0.16)",
        },
    },
});

function App() {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Router basename={process.env.PUBLIC_URL}>
                <div className="App">
                    <AppBarComponent />
                    <Container maxWidth="lg">
                        <Box my={4}>
                            <div className="AppMainComponent">
                                <RouterComponent />
                            </div>
                        </Box>
                    </Container>
                </div>
            </Router>
        </ThemeProvider>
    );
}

export default App;
