const isHostReducer = (state: boolean = false, action: any): boolean => {
    switch (action.type) {
        case "UPDATE_IS_HOST":
            return action.payload;
        default:
            return state;
    }
};

export default isHostReducer;
