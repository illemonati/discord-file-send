import { DiscordUser } from "../utils/discord-utils";

const discordUsersReducer = (
    state: DiscordUser[] = [],
    action: any
): DiscordUser[] => {
    switch (action.type) {
        case "UPDATE_DISCORD_USERS":
            return action.payload;
        case "ADD_DISCORD_USER":
            const newState = state.slice();
            const newUser = action.payload as DiscordUser;
            const newStateTokens = newState.map((s) => s.token);
            if (!newStateTokens.includes(newUser.token)) newState.push(newUser);
            return newState;
        default:
            return state;
    }
};

export default discordUsersReducer;
