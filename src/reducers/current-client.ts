const Discord = (window as any).Discord;

const currentClientReducer = (
    state: Discord.Client | null = null,
    action: any
): Discord.Client | null => {
    switch (action.type) {
        case "UPDATE_CURRENT_CLIENT":
            return action.payload;
        default:
            return state;
    }
};

export default currentClientReducer;
