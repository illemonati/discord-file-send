import { combineReducers } from "redux";
import currentClientReducer from "./current-client";
import currentRecipientReducer from "./current-recipient";
import discordUsersReducer from "./discord-users";
import isHostReducer from "./is-host";

const rootReducer = combineReducers({
    discordUsers: discordUsersReducer,
    currentClient: currentClientReducer,
    currentRecipient: currentRecipientReducer,
    isHost: isHostReducer,
});

export default rootReducer;
