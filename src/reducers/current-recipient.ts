const Discord = (window as any).Discord;

const currentRecipientReducer = (
    state: Discord.User | null = null,
    action: any
): Discord.User | null => {
    switch (action.type) {
        case "UPDATE_CURRENT_RECIPIENT":
            return action.payload;
        default:
            return state;
    }
};

export default currentRecipientReducer;
