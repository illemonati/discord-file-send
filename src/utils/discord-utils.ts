const Discord = (window as any).Discord;

export const login = async (token: string): Promise<Discord.Client> => {
    const client = new Discord.Client();
    try {
        await client.login(token);
        return client;
    } catch (err) {
        throw err;
    }
};

export const initiateSendRequest = async (recipient: Discord.User) => {
    const dm = await recipient.createDM();
    const embed = new Discord.RichEmbed();
    embed.color = 0x7289da;
    embed.title = "DFS File Send Request";
    embed.description =
        "This is a file send request made with https://illemonti.gitlab.io/discord-file-send";
    return dm.send(embed);
};

export const sendWebrtcOffer = async (
    recipient: Discord.User,
    offerString: string
) => {
    const dm = await recipient.createDM();
    const embed = new Discord.RichEmbed();
    embed.color = 0x7289da;
    embed.title = "DFS Webrtc Offer";
    embed.description =
        "This is a file send request made with https://illemonti.gitlab.io/discord-file-send";
    embed.addField("offer", offerString, false);
    return dm.send(embed);
};

export const sendWebrtcAnswer = async (
    recipient: Discord.User,
    answerString: string
) => {
    const dm = await recipient.createDM();
    const embed = new Discord.RichEmbed();
    embed.color = 0x7289da;
    embed.title = "DFS Webrtc Answer";
    embed.description =
        "This is a file send request made with https://illemonti.gitlab.io/discord-file-send";
    embed.addField("answer", answerString, false);
    return dm.send(embed);
};

export interface DiscordUser {
    token: string;
    username: string;
    avatarURL: string;
}
