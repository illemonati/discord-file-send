import {
    Box,
    Button,
    Container,
    Grid,
    Paper,
    Typography,
} from "@material-ui/core";
import prettyBytes from "pretty-bytes";
import React, { ChangeEvent, useRef, useState } from "react";
import { bytesPerChunk, chunksPerPartition } from "../configs.json";
import SendIcon from "@material-ui/icons/Send";
import PublishIcon from "@material-ui/icons/Publish";

interface FileSendComponentProps {
    dataChannel: RTCDataChannel | null;
}

const FileSendComponent = (props: FileSendComponentProps) => {
    const dataChannel = props.dataChannel;
    const fileUploadInput = useRef<HTMLInputElement | null>(null);
    const [file, setFile] = useState<File | null>(null);
    const currentChunk = useRef<number>(0);

    const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
        setFile((file) => {
            if (e.target.files && e.target.files.length > 0) {
                return e.target.files[0];
            } else {
                return file;
            }
        });
    };

    const sendFile = async () => {
        console.log(file);
        if (!file) return;
        if (!dataChannel) return;
        const uploadingFileInfo = {
            fileName: file.name,
            fileSize: file.size,
        };
        // const fileBuffer = await this.fileToSend.arrayBuffer();
        // fileChannel.send(fileBuffer);
        currentChunk.current = 0;
        const fileReader = new FileReader();
        dataChannel.send(JSON.stringify(uploadingFileInfo));
        const readNextChunk = () => {
            const start = bytesPerChunk * currentChunk.current;
            const end = Math.min(file.size, start + bytesPerChunk);
            fileReader.readAsArrayBuffer(file.slice(start, end));
        };

        const waitForReceiveConfirmation = async (chunkToCheck: number) => {
            await new Promise((r) => {
                const handlerFunction = (e: MessageEvent) => {
                    if (e.data === chunkToCheck.toString()) {
                        r(null);
                        dataChannel.removeEventListener(
                            "message",
                            handlerFunction
                        );
                    }
                };
                dataChannel.addEventListener("message", handlerFunction);
            });
        };

        fileReader.onload = async () => {
            dataChannel.send(fileReader.result as ArrayBuffer);
            currentChunk.current++;
            if (currentChunk.current % chunksPerPartition === 0) {
                await waitForReceiveConfirmation(currentChunk.current);
            }
            if (bytesPerChunk * currentChunk.current < file.size) {
                readNextChunk();
            }
        };
        readNextChunk();
    };

    return (
        <div className="FileSendComponent">
            <Paper variant="outlined">
                <Container>
                    <Box my={2}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle1">
                                    Send File
                                </Typography>
                            </Grid>
                            {file && (
                                <Grid item xs={6}>
                                    <Typography variant="subtitle2">
                                        {file.name}
                                    </Typography>
                                </Grid>
                            )}
                            {file && (
                                <Grid item xs={6}>
                                    <Typography variant="subtitle2">
                                        {prettyBytes(file.size)}
                                    </Typography>
                                </Grid>
                            )}
                            <Grid item xs={6}>
                                <input
                                    accept="*"
                                    id="file-upload"
                                    style={{ display: "none" }}
                                    type="file"
                                    ref={fileUploadInput}
                                    onChange={handleFileChange}
                                />
                                <label htmlFor="file-upload">
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        component="span"
                                        endIcon={<PublishIcon />}
                                    >
                                        Select File
                                    </Button>
                                </label>
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    endIcon={<SendIcon />}
                                    onClick={sendFile}
                                >
                                    Send
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Container>
            </Paper>
        </div>
    );
};

export default FileSendComponent;
