import {
    Box,
    Button,
    Container,
    Grid,
    TextField,
    Typography,
} from "@material-ui/core";
import React, { ChangeEvent, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import config from "./configs.json";

import SendIcon from "@material-ui/icons/Send";

import { sendWebrtcAnswer, sendWebrtcOffer } from "../../utils/discord-utils";
import FileSendComponent from "./FileSendComponent/FileSendComponent";
import FileReceiveComponent from "./FileReceiveComponent/FileReceiveComponent";

const Discord = (window as any).Discord;

const MainFileSendComponent = () => {
    const iceServers = config.iceServers;
    const [currentRecipient] = useState(
        useSelector<any, Discord.User>((state) => state.currentRecipient)
    );
    const [currentClient] = useState(
        useSelector<any, Discord.Client>((state) => state.currentClient)
    );
    const [isHost] = useState(
        useSelector<any, boolean>((state) => state.isHost)
    );

    const messagesTextField = useRef<HTMLTextAreaElement | null>(null);
    const [messageToSend, setMessageToSend] = useState("");
    const [messages, setMessages] = useState("");
    const connection = useRef<RTCPeerConnection | null>(null);
    const dataChannel = useRef<RTCDataChannel | null>(null);
    const fileChannel = useRef<RTCDataChannel | null>(null);
    const file2Channel = useRef<RTCDataChannel | null>(null);

    const handleTextMessage = (event: MessageEvent<any>): any => {
        console.log(event);
        setMessages((msgs) => `${msgs}\n\nPeer: ${event.data}`);
        if (messagesTextField.current) {
            messagesTextField.current.scrollTop =
                messagesTextField.current.scrollHeight;
        }
    };

    const sendMessage = () => {
        setMessageToSend((message) => {
            if (!dataChannel.current) return message;
            dataChannel.current.send(message);
            setMessages((msgs) => `${msgs}\n\nSelf: ${message}`);
            if (messagesTextField.current) {
                messagesTextField.current.scrollTop =
                    messagesTextField.current.scrollHeight;
            }
            return "";
        });
    };

    const handleMessageToSendChange = (
        event: ChangeEvent<HTMLInputElement>
    ) => {
        setMessageToSend(() => event.target.value);
    };

    useEffect(() => {
        const createConnectionIfNotExists = () => {
            if (connection.current) return;
            const newConn = new RTCPeerConnection({
                iceServers,
            });
            newConn.onconnectionstatechange = (event) => {
                console.log(event);
                console.log(newConn);
                if (newConn.connectionState === "connected") {
                    console.log("PEER IS connected");
                }
            };
            newConn.oniceconnectionstatechange = () => {
                console.log(newConn.iceConnectionState);
                console.log(newConn.localDescription?.sdp);
                if (newConn) {
                    console.log(newConn.iceConnectionState);
                    if (newConn.iceConnectionState === "connected") {
                        console.log("ICE IS connected");
                    }
                }
            };
            connection.current = newConn;
        };

        const makeOffer = async () => {
            createConnectionIfNotExists();
            if (!connection.current) return;
            dataChannel.current = connection.current.createDataChannel(
                "dataChannel"
            );
            fileChannel.current = connection.current.createDataChannel(
                "fileChannel"
            );
            file2Channel.current = connection.current.createDataChannel(
                "file2Channel"
            );
            fileChannel.current.binaryType = "arraybuffer";
            file2Channel.current.binaryType = "arraybuffer";
            dataChannel.current.onmessage = handleTextMessage;
            const offer = await connection.current.createOffer();
            await connection.current.setLocalDescription(offer);
            return await new Promise((r) => {
                connection.current!.onicecandidate = (e) => {
                    console.log(e);
                    if (!e.candidate) {
                        console.log("done");
                        console.log(connection.current?.localDescription?.sdp);
                        r(connection.current?.localDescription);
                    }
                };
            });
        };

        const acceptOffer = async (offerString: string) => {
            const offer = JSON.parse(offerString);
            createConnectionIfNotExists();
            if (!connection.current) return;
            connection.current.setRemoteDescription(offer);
            connection.current.onicecandidate = (e) => {
                console.log(e);

                if (!e.candidate) {
                    console.log(connection.current?.localDescription?.sdp);
                } else {
                    // peerConnection.addIceCandidate(e.candidate);
                }
            };
            const answer = await connection.current.createAnswer();
            await connection.current.setLocalDescription(answer);

            connection.current.ondatachannel = (e) => {
                console.log(e);
                if (e.channel.label === "dataChannel") {
                    dataChannel.current = e.channel;
                    if (dataChannel.current)
                        dataChannel.current.onmessage = handleTextMessage;

                    console.log("data Connected");
                } else if (e.channel.label === "fileChannel") {
                    file2Channel.current = e.channel;
                    e.channel.binaryType = "arraybuffer";
                    console.log("og file which is now file 2 Connected");
                } else if (e.channel.label === "file2Channel") {
                    fileChannel.current = e.channel;
                    e.channel.binaryType = "arraybuffer";
                    console.log("og file2 which is now file Connected");
                }
            };
            return answer;
        };

        const acceptAnswer = async (answerString: string) => {
            console.log(answerString);
            const answer = JSON.parse(answerString);
            createConnectionIfNotExists();
            answer && connection.current?.setRemoteDescription(answer);
        };

        const waitForOfferString = (): Promise<string> => {
            return new Promise((r) => {
                currentClient.on("message", (msg) => {
                    if (msg.author.id !== currentRecipient.id) return;
                    if (msg.embeds.length < 0) return;
                    const firstEmbed = msg.embeds[0];
                    if (firstEmbed.fields.length < 1) return;
                    const offerFields = firstEmbed.fields.filter(
                        (field) => field.name === "offer"
                    );
                    if (offerFields.length < 0) return;
                    const offerString = offerFields[0].value;
                    currentClient.removeAllListeners();
                    r(offerString);
                });
            });
        };

        const waitForAnswerString = (): Promise<string> => {
            return new Promise((r) => {
                currentClient.on("message", (msg) => {
                    if (msg.author.id !== currentRecipient.id) return;
                    if (msg.embeds.length < 0) return;
                    const firstEmbed = msg.embeds[0];
                    if (firstEmbed.fields.length < 1) return;
                    const fields = firstEmbed.fields.filter(
                        (field) => field.name === "answer"
                    );
                    if (fields.length < 0) return;
                    const answerString = fields[0].value;
                    currentClient.removeAllListeners();
                    r(answerString);
                });
            });
        };

        const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

        (async () => {
            if (isHost) {
                // await sleep(3000);
                const offerString = JSON.stringify(await makeOffer());
                console.log(offerString);
                sendWebrtcOffer(currentRecipient, offerString);
                const answerString = await waitForAnswerString();
                await acceptAnswer(answerString);
                console.log(dataChannel.current);
                await sleep(5000);
                dataChannel.current?.send("hello");
            } else {
                const offerString = await waitForOfferString();
                const answer = await acceptOffer(offerString);
                const answerString = JSON.stringify(answer);
                await sendWebrtcAnswer(currentRecipient, answerString);
            }
        })();
    }, [currentRecipient, isHost, currentClient, iceServers]);

    return (
        <div>
            <Container>
                <Box my={4}>
                    <Grid container spacing={3} justify="center">
                        <Grid item xs={12}>
                            <Typography variant="h5">File Send</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                value={messages}
                                fullWidth
                                multiline
                                rows={6}
                            />
                        </Grid>

                        <Grid item xs={10}>
                            <TextField
                                value={messageToSend}
                                onChange={handleMessageToSendChange}
                                variant="outlined"
                                label="Type here to send message"
                                onKeyDown={(e) =>
                                    e.key === "Enter" && sendMessage()
                                }
                                inputProps={{ ref: messagesTextField }}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                onClick={sendMessage}
                                endIcon={<SendIcon />}
                            >
                                Send
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <FileSendComponent dataChannel={fileChannel.current} />
                    </Grid>
                    <Grid item xs={6}>
                        <FileReceiveComponent
                            dataChannel={file2Channel.current}
                        />
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
};

export default MainFileSendComponent;
