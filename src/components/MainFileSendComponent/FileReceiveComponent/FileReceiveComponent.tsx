import {
    Box,
    Button,
    Container,
    Grid,
    Paper,
    Typography,
} from "@material-ui/core";
import prettyBytes from "pretty-bytes";
import React, { useEffect, useRef, useState } from "react";
import GetAppIcon from "@material-ui/icons/GetApp";
import { chunksPerPartition } from "../configs.json";

interface FileReceiveComponentProps {
    dataChannel: RTCDataChannel | null;
}

interface FileInfo {
    fileName: string;
    fileSize: number;
}

const FileReceiveComponent = (props: FileReceiveComponentProps) => {
    const dataChannel = props.dataChannel;
    const fileInfo = useRef<FileInfo | null>(null);
    const fileData = useRef<ArrayBuffer[]>([]);
    const bytesReceived = useRef(0);
    const chunksReceived = useRef(0);
    const downloadInProgress = useRef(false);
    const [receivedFileUrl, setReceivedFileUrl] = useState("");

    const receiveFile = () => {
        if (!dataChannel) return;
        dataChannel.onmessage = (e) => {
            console.log(e);
            console.log(downloadInProgress.current);
            if (!downloadInProgress.current) {
                startReceiveFile(e.data);
                bytesReceived.current = 0;
            } else {
                console.log(bytesReceived.current);
                progressReceiveFile(e.data);
            }
        };
    };

    useEffect(() => {
        receiveFile();
        // eslint-disable-next-line
    }, [dataChannel]);

    const downloadFile = () => {
        console.log(receivedFileUrl, fileInfo);
        if (!receivedFileUrl || !fileInfo.current) return;
        const a = document.createElement("a");
        a.href = receivedFileUrl;
        a.download = fileInfo.current.fileName;
        a.click();
    };

    const startReceiveFile = (data: string) => {
        fileInfo.current = JSON.parse(data);
        console.log(data);
        fileData.current = [];
        bytesReceived.current = 0;
        chunksReceived.current = 0;

        downloadInProgress.current = true;
        bytesReceived.current = 0;
    };

    const progressReceiveFile = (data: ArrayBuffer) => {
        if (!fileInfo.current) return;
        if (isNaN(bytesReceived.current)) bytesReceived.current = 0;
        if (data.byteLength) bytesReceived.current += data.byteLength || 0;
        chunksReceived.current++;
        if (chunksReceived.current % chunksPerPartition === 0) {
            dataChannel?.send(chunksReceived.current.toString());
        }
        fileData.current.push(data);
        if (bytesReceived.current >= fileInfo.current.fileSize) {
            endDownload();
        }
    };

    const endDownload = () => {
        downloadInProgress.current = false;
        const file = new Blob(fileData.current);
        const url = URL.createObjectURL(file);
        setReceivedFileUrl(url);
        console.log(downloadInProgress.current);
        console.log(fileInfo);
    };

    return (
        <div className="FileSendComponent">
            <Paper variant="outlined">
                <Container>
                    <Box my={2}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle1">
                                    Receive File
                                </Typography>
                            </Grid>
                            {fileInfo.current && (
                                <Grid item xs={6}>
                                    <Typography variant="subtitle2">
                                        {fileInfo.current.fileName}
                                    </Typography>
                                </Grid>
                            )}
                            {fileInfo.current && (
                                <Grid item xs={6}>
                                    <Typography variant="subtitle2">
                                        {prettyBytes(fileInfo.current.fileSize)}
                                    </Typography>
                                </Grid>
                            )}
                            <Grid item xs={12}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    endIcon={<GetAppIcon />}
                                    onClick={downloadFile}
                                    disabled={
                                        !fileInfo.current ||
                                        fileInfo.current.fileSize >
                                            bytesReceived.current
                                    }
                                >
                                    Download
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Container>
            </Paper>
        </div>
    );
};

export default FileReceiveComponent;
