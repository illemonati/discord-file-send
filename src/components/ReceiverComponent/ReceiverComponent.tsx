import {
    Box,
    Paper,
    Container,
    Typography,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
} from "@material-ui/core";
import React, { SyntheticEvent, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import updateCurrentRecipient from "../../actions/current-recipient";
import { useHistory } from "react-router-dom";
import updateIsHost from "../../actions/is-host";

const Discord = (window as any).Discord;

const ReceiverComponent = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const handleImageError = (e: SyntheticEvent<HTMLImageElement>) => {
        e.currentTarget.src =
            "data:image/gif;base64,R0lGODlhAQABAID/ AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
    };

    const [potentialPartner, setPotentialPartner] = useState(
        [] as Discord.User[]
    );

    const [currentClient] = useState(
        useSelector<any, Discord.Client>((state) => state.currentClient)
    );

    useEffect(() => {
        dispatch(updateIsHost(false));
    }, [dispatch]);

    useEffect(() => {
        setPotentialPartner(() => []);

        if (currentClient)
            currentClient.on("message", (msg) => {
                const embeds = msg.embeds;
                if (embeds.length < 1) return;
                const firstEmbed = embeds[0];
                if (firstEmbed.title !== "DFS File Send Request") return;
                setPotentialPartner((partners) => {
                    const newPartners = [...partners];
                    newPartners.push(msg.author);
                    return newPartners;
                });
            });
    }, [currentClient]);

    const selectPartner = async (user: Discord.User) => {
        dispatch(updateCurrentRecipient(user));
        const dm = await user.createDM();
        const embed = new Discord.RichEmbed();
        embed.color = 0x7289da;
        embed.title = "DFS File Send Accept";
        embed.description =
            "This is a file send request acceptance made with https://illemonti.gitlab.io/discord-file-send";
        await dm.send(embed);
        history.push("/main-file-send");
    };

    return (
        <div className="ReceiverComponent">
            <Box my={4}>
                <Paper>
                    <Container>
                        <Box my={2}>
                            <Box my={2}>
                                <Typography variant="h5">
                                    Select receiver
                                </Typography>
                            </Box>
                            <Box my={2}>
                                <List className="contactsList">
                                    {potentialPartner.map((user, i) => {
                                        console.log(user);
                                        return (
                                            <ListItem
                                                button
                                                key={i}
                                                onClick={() =>
                                                    selectPartner(user)
                                                }
                                            >
                                                <ListItemIcon>
                                                    <img
                                                        alt=""
                                                        onError={
                                                            handleImageError
                                                        }
                                                        src={user.avatarURL}
                                                        className="listAvatar"
                                                    />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={user.tag}
                                                    className="listUserTag"
                                                />
                                            </ListItem>
                                        );
                                    })}
                                </List>
                            </Box>
                        </Box>
                    </Container>
                </Paper>
            </Box>
        </div>
    );
};

export default ReceiverComponent;
