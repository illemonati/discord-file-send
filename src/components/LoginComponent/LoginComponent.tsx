import React, { useState, ChangeEvent, SyntheticEvent } from "react";

import {
    Box,
    Button,
    Container,
    Grid,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Paper,
    TextField,
    Typography,
} from "@material-ui/core";
import { DiscordUser, login } from "../../utils/discord-utils";
import { useDispatch, useSelector } from "react-redux";
import { addDiscordUser } from "../../actions/discord-users";
import { useHistory } from "react-router-dom";
import updateCurrentClient from "../../actions/current-client";

const LoginComponent = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [inputtedToken, setInputtedToken] = useState("");
    const [pastUsers] = useState(
        useSelector<any, DiscordUser[]>((state) => state.discordUsers)
    );

    const handleLoginButton = async () => {
        await handleLogin(inputtedToken);
    };

    const handleImageError = (e: SyntheticEvent<HTMLImageElement>) => {
        e.currentTarget.src =
            "data:image/gif;base64,R0lGODlhAQABAID/ AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
    };

    const handleLogin = async (token: string) => {
        try {
            const client = await login(token);
            dispatch(
                addDiscordUser({
                    token: token,
                    username: client.user.tag,
                    avatarURL: client.user.avatarURL,
                })
            );
            dispatch(updateCurrentClient(client));
            history.push("/recepient-selection");
            console.log(client);
        } catch (e) {
            console.log(e);
        }
    };

    const handleInputtedTokenChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setInputtedToken(() => value);
    };

    return (
        <div className="LoginComponent">
            <Box my={4}>
                <Paper variant="outlined">
                    <Container>
                        <Box my={2}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <Typography variant="h5">
                                        Login with new Token
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="Token"
                                        value={inputtedToken}
                                        onChange={handleInputtedTokenChange}
                                    ></TextField>
                                </Grid>

                                <Grid item xs={12}>
                                    <Button
                                        variant="outlined"
                                        onClick={handleLoginButton}
                                    >
                                        Login
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Container>
                </Paper>
                <Box my={4}>
                    <Paper variant="outlined">
                        <Container>
                            <Box my={2}>
                                <Grid container spacing={3}>
                                    <Grid item xs={12}>
                                        <Typography variant="h5">
                                            Past Logins
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        {pastUsers.length < 1 && (
                                            <Typography variant="subtitle1">
                                                Sad... No Past Logins yet...
                                            </Typography>
                                        )}
                                        <List className="pastUsersList">
                                            {pastUsers.map((pastUser, i) => {
                                                return (
                                                    <ListItem
                                                        button
                                                        key={i}
                                                        onClick={() =>
                                                            setInputtedToken(
                                                                pastUser.token
                                                            )
                                                        }
                                                        selected={
                                                            inputtedToken ===
                                                            pastUser.token
                                                        }
                                                    >
                                                        <ListItemIcon>
                                                            <img
                                                                alt=""
                                                                onError={
                                                                    handleImageError
                                                                }
                                                                src={
                                                                    pastUser.avatarURL
                                                                }
                                                                className="listAvatar"
                                                            />
                                                        </ListItemIcon>
                                                        <ListItemText
                                                            primary={
                                                                pastUser.username
                                                            }
                                                            secondary={
                                                                pastUser.token
                                                            }
                                                            className="listUserTag"
                                                        />
                                                    </ListItem>
                                                );
                                            })}
                                        </List>
                                    </Grid>
                                </Grid>
                            </Box>
                        </Container>
                    </Paper>
                </Box>
            </Box>
        </div>
    );
};

export default LoginComponent;
