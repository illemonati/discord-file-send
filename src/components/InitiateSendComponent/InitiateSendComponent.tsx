import {
    Box,
    Button,
    Container,
    Grid,
    Paper,
    Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { initiateSendRequest } from "../../utils/discord-utils";
import { useHistory } from "react-router-dom";
import updateIsHost from "../../actions/is-host";

const Discord = (window as any).Discord;

export const InitiateSendComponent = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const [currentRecipient] = useState(
        useSelector<any, Discord.User>((state) => state.currentRecipient)
    );

    const [currentClient] = useState(
        useSelector<any, Discord.Client | null>((state) => state.currentClient)
    );

    useEffect(() => {
        dispatch(updateIsHost(true));
    }, [dispatch]);

    const initiateRequest = async () => {
        await initiateSendRequest(currentRecipient);
        currentClient?.on("message", (msg) => {
            const embeds = msg.embeds;
            if (embeds.length < 1) return;
            const firstEmbed = embeds[0];
            if (firstEmbed.title !== "DFS File Send Accept") return;
            history.push("/main-file-send");
        });
    };

    return (
        <div className="InitiateSendComponent">
            <Box my={4}>
                <Paper>
                    <Container>
                        <Box my={2}>
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <Typography variant="h5">
                                        Send to {currentRecipient?.tag}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Button
                                        variant="outlined"
                                        onClick={initiateRequest}
                                    >
                                        Initiate Request
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Container>
                </Paper>
            </Box>
        </div>
    );
};

export default InitiateSendComponent;
