import {
    Box,
    Container,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Paper,
    Typography,
} from "@material-ui/core";
import React, { SyntheticEvent, useState } from "react";

import { useDispatch, useSelector } from "react-redux";

import updateCurrentRecipient from "../../actions/current-recipient";
import { useHistory } from "react-router-dom";

const Discord = (window as any).Discord;

const RecipientSelectionComponent = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const handleImageError = (e: SyntheticEvent<HTMLImageElement>) => {
        e.currentTarget.src =
            "data:image/gif;base64,R0lGODlhAQABAID/ AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
    };

    const [currentClient] = useState(
        useSelector<any, Discord.Client>((state) => state.currentClient)
    );

    const handleRecipient = (user: Discord.User | null) => {
        if (!user) {
            console.log(user);
            history.push("/receiver-component");
            return;
        }
        dispatch(updateCurrentRecipient(user));
        history.push("/initiate-send");
    };

    return (
        <div className="RecipientSelectionComponent">
            <Box my={4}>
                <Paper>
                    <Container>
                        <Box my={2}>
                            <Box my={2}>
                                <Typography variant="h5">
                                    Select a contact to send file to
                                </Typography>
                            </Box>
                            <Box my={2}>
                                <List className="contactsList">
                                    <ListItem
                                        button
                                        onClick={() => {
                                            handleRecipient(null);
                                        }}
                                    >
                                        <ListItemIcon>
                                            <img
                                                alt=""
                                                onError={handleImageError}
                                                className="listAvatar"
                                            />
                                        </ListItemIcon>
                                        <ListItemText
                                            primary="Recieve"
                                            className="listUserTag"
                                        />
                                    </ListItem>
                                    {currentClient?.user?.friends?.map(
                                        (user, i) => {
                                            console.log(user);
                                            return (
                                                <ListItem
                                                    button
                                                    key={i}
                                                    onClick={() =>
                                                        handleRecipient(user)
                                                    }
                                                >
                                                    <ListItemIcon>
                                                        <img
                                                            alt=""
                                                            onError={
                                                                handleImageError
                                                            }
                                                            src={user.avatarURL}
                                                            className="listAvatar"
                                                        />
                                                    </ListItemIcon>
                                                    <ListItemText
                                                        primary={user.tag}
                                                        className="listUserTag"
                                                    />
                                                </ListItem>
                                            );
                                        }
                                    )}
                                </List>
                            </Box>
                        </Box>
                    </Container>
                </Paper>
            </Box>
        </div>
    );
};

export default RecipientSelectionComponent;
