import React from "react";

import { AppBar, Toolbar, Typography } from "@material-ui/core";
import "./styles.css";
const AppBarComponent = () => {
    return (
        <>
            <AppBar position="sticky" color="primary" className="mainAppBar">
                <Toolbar>
                    <Typography variant="h6" className="appBarTitle">
                        Discord File Send
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default AppBarComponent;
