import React from "react";
import { lazy, Suspense } from "react";

import { Switch, Route, Redirect } from "react-router-dom";
const InitiateSendComponent = lazy(
    () => import("./components/InitiateSendComponent/InitiateSendComponent")
);

const RecipientSelectionComponent = lazy(
    () =>
        import(
            "./components/RecipientSelectionComponent/RecipientSelectionComponent"
        )
);

const MainFileSendComponent = lazy(
    () => import("./components/MainFileSendComponent/MainFileSendComponent")
);

const ReceiverComponent = lazy(
    () => import("./components/ReceiverComponent/ReceiverComponent")
);

const LoginComponent = lazy(
    () => import("./components/LoginComponent/LoginComponent")
);

const RouterComponent = () => (
    <Suspense fallback={<></>}>
        <Switch>
            <Route exact path="/">
                <Redirect to="/login" />
            </Route>

            <Route exact path="/login">
                <LoginComponent />
            </Route>

            <Route exact path="/recepient-selection">
                <RecipientSelectionComponent />
            </Route>

            <Route exact path="/initiate-send">
                <InitiateSendComponent />
            </Route>

            <Route exact path="/receiver-component">
                <ReceiverComponent />
            </Route>

            <Route exact path="/main-file-send">
                <MainFileSendComponent />
            </Route>
        </Switch>
    </Suspense>
);

export default RouterComponent;
